# README #

### SIRIS4 ###

* Add description

### Compiling ###

* Makefile for GNU make is in 'src' directory.
* Use 'make' or 'make' with options, possible options are:
 * COMP=gcc/intel compiler family, gcc default
 * EXENAME=name name for the binary, siris4 default
 * MACH=linux/win OS environment, linux default
 * OPTI=0/1/2 optimization level, 1 default
 * PROC= processor family for which to optimize with OPTI=2, default native
* Example: 'make COMP=gcc EXENAME=siris4 MACH=linux OPTI=1' equals 'make'

### Developers ###

* Karri Muinonen, Julia Martikainen, Hannakaisa Lindqvist, Antti Penttil�